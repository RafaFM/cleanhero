//
//  Pagination.swift
//  CleanHero
//
//  Created by Rafael Ferrero on 3/5/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import UIKit

protocol PaginationViewController {
    var isLoading: Bool { get set }
    
    func shouldFetchMorePages(_ scrollView: UIScrollView, completion: @escaping ()->Void)
}

extension PaginationViewController {
    func shouldFetchMorePages(_ scrollView: UIScrollView, completion: @escaping ()->Void) {
        let offset = scrollView.contentOffset.y
        let maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maxOffset - offset) <= 5 && !isLoading{
            completion()
        }
    }
}

protocol PaginationInteractor {
    associatedtype T
    var paginationData: T { get set }
    mutating func hasMorePages() -> Bool
    func append<U>(data: [U]?, with newData: [U]) -> [U]
}
