//
//  CharacterListTableViewCell.swift
//  CleanHero
//
//  Created by Rafael Ferrero on 7/5/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import UIKit

class CharacterListTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var photoimageView: UIImageView!
    
    static let cellID = "CharacterListTableViewCell"
}
