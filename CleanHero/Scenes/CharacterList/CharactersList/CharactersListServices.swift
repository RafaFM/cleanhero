//
//  CharactersListServices.swift
//  CleanHero
//
//  Created by Rafael Ferrero on 7/5/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

extension MarvelAPIRequests {
    struct CharactersList: MarvelAPIRequestProtocol, MarvelAPIPaginationProtocol {
        
        
        var path: String { return "/characters" }
        var nameToSearch: String
        var offset: Int

        var parameters: [String : Any]? {
            var params: [String: Any]?
            
            if !nameToSearch.isEmpty {
                params = [
                    "name": nameToSearch
                ]
            }
            
            let paramsWithPaginations = concatenate(parameters: params)
            return paramsWithPaginations
        }
        
        
    }
}
