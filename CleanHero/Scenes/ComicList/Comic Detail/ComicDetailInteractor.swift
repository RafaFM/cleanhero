//
//  ComicDetailInteractor.swift
//  CleanHero
//
//  Created by Rafael Ferrero on 27/4/18.
//  Copyright (c) 2018 Rafael Ferrero. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol ComicDetailBusinessLogic
{
    func getComic(request: ComicDetail.GetComic.Request)
}

protocol ComicDetailDataStore
{
  var comic: ComicModel! { get set }
}

class ComicDetailInteractor: ComicDetailBusinessLogic, ComicDetailDataStore
{
  var presenter: ComicDetailPresentationLogic?
  var worker: ComicDetailWorker?
    var comic: ComicModel!
    
  // MARK: Do something
  
  func getComic(request: ComicDetail.GetComic.Request)
  {
    let response = ComicDetail.GetComic.Response(comic: comic)
    presenter?.presentComic(response: response)
  }
}
