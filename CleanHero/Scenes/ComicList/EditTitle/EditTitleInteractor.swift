//
//  EditTitleInteractor.swift
//  CleanHero
//
//  Created by Opentrends on 1/5/18.
//  Copyright (c) 2018 Opentrends. All rights reserved.
//

import UIKit

protocol EditTitleBusinessLogic {
    func changeTitle(request: EditTitle.Change.Request)
}

protocol EditTitleDataStore {
    var title: String { get set }
}

class EditTitleInteractor: EditTitleBusinessLogic, EditTitleDataStore {
    var presenter: EditTitlePresentationLogic?
    var title: String = ""
    
    // MARK: Do something
    
    func changeTitle(request: EditTitle.Change.Request)
    {
        title = request.title
        let response = EditTitle.Change.Response()
        presenter?.presentChangedTitle(response: response)
    }
}
