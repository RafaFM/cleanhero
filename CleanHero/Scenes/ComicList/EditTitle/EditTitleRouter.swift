//
//  EditTitleRouter.swift
//  CleanHero
//
//  Created by Opentrends on 1/5/18.
//  Copyright (c) 2018 Opentrends. All rights reserved.
//

import UIKit

@objc protocol EditTitleRoutingLogic {
    func routeToComicList(segue: UIStoryboardSegue?)
}

protocol EditTitleDataPassing {
    var dataStore: EditTitleDataStore? { get }
}

class EditTitleRouter: NSObject, EditTitleRoutingLogic, EditTitleDataPassing {
    weak var viewController: EditTitleViewController?
    var dataStore: EditTitleDataStore?
    
    // MARK: Routing
    func routeToComicList(segue: UIStoryboardSegue?)
    {
        let index = viewController!.navigationController!.viewControllers.count - 2
        let destinationVC = viewController?.navigationController?.viewControllers[index] as! ComicsListViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToParent(source: dataStore!, destination: &destinationDS)
        navigateToParent(source: viewController!, destination: destinationVC)
    }

    // MARK: Navigation
    
    func navigateToParent(source: EditTitleViewController, destination: ComicsListViewController)
    {
        source.navigationController?.popViewController(animated: true)
    }
    
    // MARK: Passing data
    
    func passDataToParent(source: EditTitleDataStore, destination: inout ComicsListDataStore)
    {
        destination.title = source.title
    }
}
