//
//  EditTitleViewController.swift
//  CleanHero
//
//  Created by Opentrends on 1/5/18.
//  Copyright (c) 2018 Opentrends. All rights reserved.
//

import UIKit

protocol EditTitleDisplayLogic: class {
    func displayChangedTitle(viewModel: EditTitle.Change.ViewModel)
}

class EditTitleViewController: UIViewController, EditTitleDisplayLogic {
    var interactor: EditTitleBusinessLogic?
    var router: (NSObjectProtocol & EditTitleRoutingLogic & EditTitleDataPassing)?

    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?){
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = EditTitleInteractor()
        let presenter = EditTitlePresenter()
        let router = EditTitleRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: Do something
    
    //@IBOutlet weak var nameTextField: UITextField!
    @IBAction func saveButtonTapped(_ sender: Any) {
        changeTitle()
    }
    
    func changeTitle() {
        let title = "titulo cambiado"
        let request = EditTitle.Change.Request(title: title)
        interactor?.changeTitle(request: request)
    }
    
    func displayChangedTitle(viewModel: EditTitle.Change.ViewModel) {
        router?.routeToComicList(segue: nil)
    }
}
