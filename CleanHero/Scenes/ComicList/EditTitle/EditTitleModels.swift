//
//  EditTitleModels.swift
//  CleanHero
//
//  Created by Opentrends on 1/5/18.
//  Copyright (c) 2018 Opentrends. All rights reserved.
//

import UIKit

enum EditTitle {
    // MARK: Use cases
  
    enum Change {
        struct Request {
            let title: String
        }
        struct Response {
        }
        struct ViewModel {
        }
  }
}
