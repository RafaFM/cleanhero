//
//  EditTitlePresenter.swift
//  CleanHero
//
//  Created by Opentrends on 1/5/18.
//  Copyright (c) 2018 Opentrends. All rights reserved.
//

import UIKit

protocol EditTitlePresentationLogic {
    func presentChangedTitle(response: EditTitle.Change.Response)
}

class EditTitlePresenter: EditTitlePresentationLogic {
    weak var viewController: EditTitleDisplayLogic?
    
    // MARK: Do something
    
    func presentChangedTitle(response: EditTitle.Change.Response)
    {
        let viewModel = EditTitle.Change.ViewModel()
        viewController?.displayChangedTitle(viewModel: viewModel)
    }
}
