//
//  AuthenticationServices.swift
//  CleanHero
//
//  Created by Rafa on 22/4/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import Foundation

extension MarvelAPIRequests {
    struct ComicsList: MarvelAPIRequestProtocol, MarvelAPIPaginationProtocol {
        var offset: Int
        var limit: Int
        
        let textToSearch: String
        var path: String { return "/comics" }

        
        var parameters: [String: Any]? {
            
            var params: [String: Any]?
            
            if !textToSearch.isEmpty {
                params = [
                    "titleStartsWith": textToSearch
                ]
            }
            
            let bla = concatenate(parameters: params)
            return bla
        }
    }
}
