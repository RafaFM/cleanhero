//
//  MarvelPaginatedInteractor.swift
//  CleanHero DEV
//
//  Created by Rafael Ferrero on 7/5/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import Foundation

struct MarvelPaginationInteractor: PaginationInteractor {
    
    struct MarvelPaginationData {
        var offset: Int = 0
        var limit: Int = 0
        var total: Int?
        var count: Int = 0
    }
    
    var paginationData = MarvelPaginationData()
    
    mutating func hasMorePages() -> Bool {
        self.paginationData.offset += self.paginationData.count
        
        if let total = self.paginationData.total {
            guard self.paginationData.offset < total else {
                return false
            }
        }
        return true
    }
    
    func getOffset() -> Int {
        return paginationData.offset
    }
    
    func append<U>(data: [U]?, with newData: [U]) -> [U] {
        var newModel = data
        if let _ = data {
            newModel!.append(contentsOf: newData)
        } else {
            newModel = newData
        }
        return newModel!
    }
    
    func paginationDataWitNew(limit: Int, count: Int, total: Int) -> MarvelPaginationInteractor {
        return MarvelPaginationInteractor(paginationData: MarvelPaginationData(offset: paginationData.offset,
                                                                               limit: limit,
                                                                               total: total,
                                                                               count: count))
    }
}
