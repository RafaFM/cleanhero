//
//  ImageDownloaderWorker.swift
//  CleanHero
//
//  Created by Rafael Ferrero on 16/5/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import Foundation
import SDWebImage

class ImageDownloaderWorker {
    func loadImage(to imageView: UIImageView, from url: URL?, with placeholder: UIImage? = nil) {
        imageView.sd_setShowActivityIndicatorView(true)
        imageView.sd_setIndicatorStyle(.gray)
        imageView.sd_setImage(with: url,
                              placeholderImage: placeholder)
    }
}
