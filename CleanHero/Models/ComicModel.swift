//
//  ComicModel.swift
//  CleanHero
//
//  Created by Rafa on 22/4/18.
//  Copyright © 2018 TheBoxApps. All rights reserved.
//

import Foundation

protocol MarvelApiListProtocol: Codable {
    associatedtype T
    
    var offset: Int { get set }
    var limit: Int { get set }
    var total: Int { get set }
    var count: Int { get set }
    var results: [T] { get set }
}

struct ComicDataModel: MarvelApiListProtocol {
    var offset: Int
    var limit: Int
    var total: Int
    var count: Int
    var results: [ComicModel]
}

struct ComicModel: Codable {
    let id: Int
    let title: String
    let isbn: String
    let modified: String
    let issueNumber: Int
    
//    enum CodingKeys: String, CodingKey
//    {
//        case id
//        case titulo = "title"
//    }
    
}

//extension ComicModel: Parsable {
//    static func parseObject(from json: [AnyHashable: Any]) -> ComicModel? {
//        guard let id = json["id"] as? Int,
//            let title = json["title"] as? String,
//            let modifiedDate = json["modified"] as? String,
//            let issueNumber = json["issueNumber"] as? Int else {
//                return nil
//        }
//
//        return ComicModel(id: id,
//                          title: title,
//                          modifiedDate: modifiedDate,
//                          issueNumber: issueNumber)
//    }
//}
/*
{
    "code": 200,
    "status": "Ok",
    "copyright": "© 2018 MARVEL",
    "attributionText": "Data provided by Marvel. © 2018 MARVEL",
    "attributionHTML": "<a href=\"http://marvel.com\">Data provided by Marvel. © 2018 MARVEL</a>",
    "etag": "1d7f74becbcdd94e24f78e9fcfb7e1b9af5392e6",
    "data": {
        "offset": 0,
        "limit": 20,
        "total": 1,
        "count": 1,
        "results": [
        {
        "id": 22253,
        "digitalId": 0,
        "title": "Hulk Custom Comic (2008) #1",
        "issueNumber": 1,
        "variantDescription": "",
        "description": null,
        "modified": "-0001-11-30T00:00:00-0500",
        "isbn": "",
        "upc": "",
        "diamondCode": "",
        "ean": "",
        "issn": "",
        "format": "Comic",
        "pageCount": 0,
        "textObjects": [],
        "resourceURI": "http://gateway.marvel.com/v1/public/comics/22253",
        "urls": [
        {
        "type": "detail",
        "url": "http://marvel.com/comics/issue/22253/hulk_custom_comic_2008_1?utm_campaign=apiRef&utm_source=a3543994f37f1b53d28727494ff667c9"
        }
        ],
        "series": {
        "resourceURI": "http://gateway.marvel.com/v1/public/series/5865",
        "name": "Hulk Custom Comic (2008)"
        },
        "variants": [],
        "collections": [],
        "collectedIssues": [],
        "dates": [
        {
        "type": "onsaleDate",
        "date": "2029-12-31T00:00:00-0500"
        },
        {
        "type": "focDate",
        "date": "-0001-11-30T00:00:00-0500"
        }
        ],
        "prices": [
        {
        "type": "printPrice",
        "price": 0
        }
        ],
        "thumbnail": {
        "path": "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available",
        "extension": "jpg"
        },
        "images": [],
        "creators": {
        "available": 0,
        "collectionURI": "http://gateway.marvel.com/v1/public/comics/22253/creators",
        "items": [],
        "returned": 0
        },
        "characters": {
        "available": 1,
        "collectionURI": "http://gateway.marvel.com/v1/public/comics/22253/characters",
        "items": [
        {
        "resourceURI": "http://gateway.marvel.com/v1/public/characters/1009351",
        "name": "Hulk"
        }
        ],
        "returned": 1
        },
        "stories": {
        "available": 2,
        "collectionURI": "http://gateway.marvel.com/v1/public/comics/22253/stories",
        "items": [
        {
        "resourceURI": "http://gateway.marvel.com/v1/public/stories/50568",
        "name": "Cover #50568",
        "type": "cover"
        },
        {
        "resourceURI": "http://gateway.marvel.com/v1/public/stories/50569",
        "name": "Interior #50569",
        "type": "interiorStory"
        }
        ],
        "returned": 2
        },
        "events": {
        "available": 0,
        "collectionURI": "http://gateway.marvel.com/v1/public/comics/22253/events",
        "items": [],
        "returned": 0
        }
        }
        ]
    }
}
*/
