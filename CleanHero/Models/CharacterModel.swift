//
//  CharacterModel.swift
//  CleanHero
//
//  Created by Rafael Ferrero on 7/5/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import Foundation
struct CharacterDataModel: MarvelApiListProtocol {
    
    var offset: Int
    var limit: Int
    var total: Int
    var count: Int
    var results: [CharacterModel]
}
struct CharacterModel: Codable {
    let id: Int
    let name: String
    let description: String
    let thumbnail: Thumbnail
    
    func getThumbnailUrl() -> URL? {
        return URL(string: "\(thumbnail.path)/standard_fantastic.\(thumbnail.ext)")
    }
}

struct Thumbnail: Codable {
    let path: String
    let ext: String
    
    enum CodingKeys: String, CodingKey {
        case path
        case ext = "extension"
    }
}
