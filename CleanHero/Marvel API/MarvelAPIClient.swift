//
//  NetworkClient.swift
//  CleanHero
//
//  Created by Rafa on 21/4/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import Foundation
import Alamofire

class MarvelAPIClient: APIClientProtocol {
    typealias Request = MarvelAPIRequestProtocol
    typealias ResponseCompletionHandler = Result<Decodable, MarvelAPIError>
    
    var authenticationObject: MarvelAPIAuthenticationData?
    var baseURL: URL = MarvelBaseURL().url
    
    static let shared = MarvelAPIClient(apiURL: MarvelBaseURL(), authenticationData: MarvelAPIAuthenticationData())
    private let alamofireManager: SessionManager
    
    
    private init(apiURL: MarvelBaseURL, authenticationData: MarvelAPIAuthenticationData) {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        
        alamofireManager = Alamofire.SessionManager(configuration: configuration)
        
        authenticationObject = authenticationData
    }
    
    func performCall<U: Decodable>(with request: MarvelAPIRequestProtocol, response: U.Type, completion: @escaping (ResponseCompletionHandler) -> ()) {
        switch request.httpMethod {
            
        case .GET:
            getRequest(with: request, response: response, completion: completion)
        case .POST:
            print("post")
        }
    }

    func authenticate(_ url: inout URL, with authenticationObject: MarvelAPIAuthenticationData?) {
        
        guard let authenticationParameters = authenticationObject?.authenticationInfo.authenticationParameters else {
            return
        }
        
        var components = URLComponents(url: url,
                                       resolvingAgainstBaseURL: false)
        
        components?.queryItems = authenticationParameters.map ( { URLQueryItem(name: String($0), value: String(describing: $1)) } )
        
        url = components!.url!
    }
}


extension MarvelAPIClient {
    
    private func getEndPoint(to resource: MarvelAPIRequestProtocol) -> URL? {
        return self.baseURL.appendingPathComponent(resource.path)
    }
    
    private func setParameters(for finalURL: inout URL, from resource: MarvelAPIRequestProtocol) {
        guard let parameters = resource.parameters else {
            return
        }
        
        var components = URLComponents(url: finalURL,
                                       resolvingAgainstBaseURL: false)
        
        let queryItems = parameters.map( { URLQueryItem(name: String($0), value: String(describing: $1)) } )
        
        if let _ = components?.queryItems {
            components?.queryItems?.append(contentsOf: queryItems)
        } else {
            components?.queryItems = queryItems
        }
        
        finalURL = components!.url!
    }
    
    private func getFinalUrl(to resource: MarvelAPIRequestProtocol) -> URL {
        var url = self.getEndPoint(to: resource)!
        self.authenticate(&url, with: self.authenticationObject)
        self.setParameters(for: &url, from: resource)
        
        return url
    }
    
    private func getRequest<T: Decodable>(with resource: MarvelAPIRequestProtocol, response: T.Type, completion: @escaping (ResponseCompletionHandler) -> ()) {
        let finalUrl = getFinalUrl(to: resource)
        
        alamofireManager.request(finalUrl)
            .validate()
            .responseJSON { (response) in
                guard response.result.isSuccess else {
                    completion(Result.failure(.responseUnsuccessful))
                    return
                }
                
                guard let json = response.result.value as? [String:Any],
                    let data = json["data"] as? [AnyHashable: Any],
                    let jsonData = try? JSONSerialization.data(withJSONObject: data),
                    let results = try? JSONDecoder().decode(T.self, from: jsonData) else {
                        completion(Result.failure(.jsonConversionFailure))
                        return
                }
                
                completion(Result.success(results))
        }
    }
    

}
