//
//  MarvelAPIErrors.swift
//  CleanHero DEV
//
//  Created by Rafa on 1/5/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import Foundation

enum MarvelAPIError: Error {
    case responseUnsuccessful
    case jsonConversionFailure
    
    var localizedDescription: String {
        switch self {
        case .responseUnsuccessful: return "Response Unsuccessful"
        case .jsonConversionFailure: return "JSON Conversion Failure"
        }
    }
}
