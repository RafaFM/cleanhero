//
//  MarvelAPIUtils.swift
//  CleanHero DEV
//
//  Created by Rafa on 25/4/18.
//  Copyright © 2018 TheBoxApps. All rights reserved.
//

import Foundation

struct MarvelBaseURL {
    var url: URL {
        get {
            #if DEVELOP
                return URL(string: "http://gateway.marvel.com/v1/public")!
            #elseif PRE
                return URL(string: "https://gateway.marvel.com/v1/public")!
            #else
                return URL(string: "https://url.dePRO/v1/public")!
            #endif
        }
        set {
            url = newValue
        }
    }
}
