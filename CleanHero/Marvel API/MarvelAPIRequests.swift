//
//  MarvelAPIRequests.swift
//  CleanHero
//
//  Created by Rafa on 21/4/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import Foundation

enum Method {
    case GET
    case POST
}

protocol MarvelAPIRequestProtocol {
    var path: String { get }
    var httpMethod: Method { get }
    var parameters: [String: Any]? { get }
}

extension MarvelAPIRequestProtocol {
    var httpMethod: Method { return .GET }
    
    var parameters: [String: Any]? {
        return [:]
    }
}

protocol MarvelAPIPaginationProtocol {
    var limit: Int { get }
    var offset: Int { get }
    var paginationParameters: [String: Any]? { get }
}

extension MarvelAPIPaginationProtocol {
    var limit: Int { return 20 }
    
    var paginationParameters: [String: Any]? {
        return [
            "limit": limit,
            "offset": offset
        ]
    }
    
    func concatenate(parameters: [String: Any]?) -> [String: Any]? {
        
        guard let params = parameters else {
            return paginationParameters
        }
        
        guard var pagParams = paginationParameters else {
            return parameters
        }
        
        params.forEach{ pagParams[$0] = $1 }
        
        return pagParams
    }
}

struct MarvelAPIRequests {
    // Extend this struct for each scene
    // The new nested structs has to conform APIRequestProtocol
}
