//
//  ComicModel.swift
//  CleanHero
//
//  Created by Rafa on 22/4/18.
//  Copyright © 2018 TheBoxApps. All rights reserved.
//

import Foundation

protocol ComicProtocol: Codable {
    var id: Int { get }
    var title: String { get }
}


struct Comic: ComicProtocol {
    let id: Int
    let title: String
}
/*
 "id": 37497,
 "digitalId": 0,
 "title": "Marvels Vol. 1 (1994) #1",
 "issueNumber": 1,
 "variantDescription": "",
 "description": null,
 "modified": "2014-05-08T12:18:41-0400",
 "isbn": "",
 "upc": "",
 "diamondCode": "",
 "ean": "",
 "issn": "",
 "format": "Digital Comic",
 "pageCount": 0,
 "textObjects": [],
 "resourceURI": "http://gateway.marvel.com/v1/public/comics/37497",
 "urls": [
 {
 "type": "detail",
 "url": "http://marvel.com/comics/issue/37497/marvels_vol_1_1994_1?utm_campaign=apiRef&utm_source=a3543994f37f1b53d28727494ff667c9"
 }
 ],
 "series": {
 "resourceURI": "http://gateway.marvel.com/v1/public/series/13495",
 "name": "Marvels Vol. 1 (1994)"
 },
 "variants": [],
 "collections": [],
 "collectedIssues": [],
 "dates": [
 {
 "type": "onsaleDate",
 "date": "2029-12-31T00:00:00-0500"
 },
 {
 "type": "focDate",
 "date": "-0001-11-30T00:00:00-0500"
 }
 ],
 "prices": [
 {
 "type": "printPrice",
 "price": 0
 }
 ],
 "thumbnail": {
 "path": "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available",
 "extension": "jpg"
 },
 "images": [],
 "creators": {
 "available": 0,
 "collectionURI": "http://gateway.marvel.com/v1/public/comics/37497/creators",
 "items": [],
 "returned": 0
 },
 "characters": {
 "available": 0,
 "collectionURI": "http://gateway.marvel.com/v1/public/comics/37497/characters",
 "items": [],
 "returned": 0
 },
 "stories": {
 "available": 0,
 "collectionURI": "http://gateway.marvel.com/v1/public/comics/37497/stories",
 "items": [],
 "returned": 0
 },
 "events": {
 "available": 0,
 "collectionURI": "http://gateway.marvel.com/v1/public/comics/37497/events",
 "items": [],
 "returned": 0
 }
 },
 */
