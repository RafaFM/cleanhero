//
//  MarvelAuthenticationData.swift
//  CleanHero
//
//  Created by Rafa on 27/4/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import Foundation
import CryptoSwift

struct MarvelAPIAuthenticationData: AuthenticationDataProtocol {
    typealias T = Credentials
    
    struct Credentials {
        var privateKey = "aeb78829714ebd706314728dfba1bd42ea3eaf77"
        var publicKey = "a3543994f37f1b53d28727494ff667c9"
        
        private let timeStamp: String = {
            return NSDate().timeIntervalSince1970.description
        }()
        
        var authenticationParameters: [String : Any] {
            let hashable = "\(timeStamp)\(privateKey)\(publicKey)"
            
            return [
                "ts": timeStamp,
                "apikey": publicKey,
                "hash": hashable.md5()
            ]
        }
    }
    
    var authenticationInfo = Credentials()
}
