//
//  APIAuthenticationDataProtocol.swift
//  CleanHero
//
//  Created by Rafa on 27/4/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import Foundation

protocol AuthenticationDataProtocol{
    associatedtype T
    var authenticationInfo: T { get set }
}
