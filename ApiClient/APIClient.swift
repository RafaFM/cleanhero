//
//  APIClient.swift
//  CleanHero
//
//  Created by Rafa on 27/4/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import Foundation

protocol APIClientProtocol {
    associatedtype T = AuthenticationDataProtocol
    associatedtype ResponseCompletionHandler
    associatedtype Request

    var authenticationObject: T? { get set }
    var baseURL: URL { get set }

    func performCall<U: Decodable>(with request: Request, response: U.Type, completion: @escaping (ResponseCompletionHandler) -> () )
    func authenticate(_ url: inout URL, with authenticationObject: T?)
}
