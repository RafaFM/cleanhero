//
//  Result.swift
//  CleanHero
//
//  Created by Rafa on 21/4/18.
//  Copyright © 2018 Rafael Ferrero. All rights reserved.
//

import Foundation

enum Result<T, U: Error> {
    case success(T)
    case failure(U)
}

